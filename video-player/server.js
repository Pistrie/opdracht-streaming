const express = require("express");
const fs = require("fs");

const app = express();

app.get("/video", (req, res) => {
  const range = req.headers.range;
  const videoPath = "./video-stream/video.webm";
  const videoSize = fs.statSync(videoPath).size;

  const chunkSize = 1 * 1e6; // 1 MB
  const start = Number(range.replace(/\D/g, ""));
  const end = Math.min(start + chunkSize, videoSize - 1);

  const contentLength = end - start + 1;

  const headers = {
    "Content-range": `bytes ${start}-${end}/${videoSize}`,
    "Accept-Ranges": "bytes",
    "Content-Length": contentLength,
    "Content-Type": "video/webm",
  };
  res.writeHead(206, headers);

  const stream = fs.createReadStream(videoPath, { start, end });
  stream.pipe(res);
});

app.listen("3000");
